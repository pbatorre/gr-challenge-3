Create a fourth table `transactions` that has the following attributes:

    amount - Equivalent to the purchase amount;
             This value is positive if the transaction is
             for a new purchase, and negative if it's for a
             refund
    seller_id - ID of the seller whose product was purchased
    purchased_at - Timestamp when the purchase was made
    purchase_id - ID of the purchase
    completed_at - Timestamp when the transaction was computed for a pay out

Create a [**partial index**](https://www.postgresql.org/docs/9.3/indexes-partial.html) on `completed_at` and `seller_id`:

`CREATE INDEX transactions_completed_at_seller_id_idx ON transactions (completed_at, seller_id) WHERE completed_at IS NULL;`

## Demo

### Given

I. We have a product **Foo** with the following  attributes:

    seller_id = 1
    price = 10

II. One **Foo** was purchased on **January 10, 2019** with purchase ID equal to 2.

### Workflow

a)  As soon as the purchase above was made, create an entry in the `transactions` table:

    purchase_id = 2
    purchased_at = <January 10, 2019>
    amount = 10
    seller_id = 1

b) A refund was issued for purchase with ID 2 on **January 17, 2019**. Create another entry in `transactions`:

    purchase_id = 2
    purchased_at = <January 10, 2019>
    amount = -10
    seller_id = 1

Note: The refund transaction looks almost exactly like the purchase transaction except for the amount.

c) If we want to pay seller with ID 1 on **January 20, 2019** for their sales up to **January 12, 2019**, execute this query:

    SELECT sum(amount)
    FROM transactions
    WHERE completed_at IS NULL
    AND seller_id = 1
    AND purchased_at <= <January 12, 2019>;

Notes:

1. The result of the above query **must be 0.**
2. `purchased_at` value comes from a cron job. We can use an environment variable to anchor its value to the current datetime, e.g. 1 week ago, 2 weeks ago, etc

d) Once we've paid seller with ID 1, set the `completed_at` of the transactions picked up from step c, with the current timestamp.

### Rationale

1. `transactions` table was created to make it possible to compute for a pay out without table joins.
2. Each purchase and its refund exist as separate entries in `transactions` to simplify the computation for the payout. We store positive and negative amounts as is so we won't need to infer the sign based on another field.
3. `transactions_completed_at_seller_id_idx ` was created to significantly reduce the number of transactions we scan when we want to make a pay out. If the last payout was 2 weeks ago, the subset of transactions **without** a value for `completed_at` will be small.
